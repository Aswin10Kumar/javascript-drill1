
function problem1(inventory){
    let carID=33;
    const car=inventory[carID-1];
    const carDetails=`Car 33 is ${car.car_year} ${car.car_make} ${car.car_model}`;
    return carDetails;
}

module.exports={problem1}

// export default { problem1 };

//console.log(carDetails);


//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"